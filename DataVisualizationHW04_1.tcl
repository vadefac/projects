# Brock Pivirotto
# CSCI B466
# 12/14/2017

# Step 11 (Why the "displacement plot" method is better the method implmented
#                    in "vtkElevationFilter" for this application?)
# ################################
# VtkVectorDot creates the scalar value by computing the dot profuct between 
# the normal and vector at each point. While vtkElevationFilter creates the scalar value by 
# computing a projection from each dataset onto point onto a line. 
# ################################


package require vtk
package require vtkinteraction

# --------- Prepare and process the data -----------------------
# Read a vtk file
#   The output of this reader is a single vtkPolyData data object
#   and it can be accessed by [plate GetOutput]
vtkPolyDataReader plate
    plate SetFileName "plate.vtk"
	#Set the name of the vector data to extract
	#  The vector data can be found in the data file
	#  There are 5 sets of vector data, i.e., mode1, mode2, mode3,
	#     mode4, and mode5
	#  Here we use the vector data named as "mode1"
    plate SetVectorsName "mode1"

#vtkWarpVector is a filter that modifies point coordinates by moving
#  points along vector times the scale factor. Useful for showing flow
#  profiles or mechanical deformation.
vtkWarpVector warp
    warp SetInput [plate GetOutput]
	# A positive factor (0.9) means the motion is in the direction of
	#   the surface normal (i.e., positive displacement)
    warp SetScaleFactor 5.0
	# A negtive factor (-0.9) means the motion is in the opposite
	#   direction of the surface normal (i.e., negative displacement)
	#warp SetScaleFactor -0.9
	
#vtkPolyDataNormals is a filter that computes point normals for a
#  polygonal mesh
vtkPolyDataNormals normals
    normals SetInput [warp GetOutput]

#vtkVectorDot is a filter to generate scalar values from a dataset
#  The scalar value at a point is created by computing the dot product
#  between the normal and vector at that point 
#  i.e. the "displacement plots" algorithm introduced in Lecture10
vtkVectorDot color
    color SetInput [normals GetOutput]


# -------- generate scalar values using Elevation algorithm ------------
#Step 2: Generate the color scalar value for each point from 
#        its vector using “vtkElevationFilter” which is
#        covered in Lecture08 and used in "hawaii.tcl"
#Step 3: Comment out the statements created in step 2 (but keep the code
#        for grading purpose) so the code would be the same as that
#        before step 2.
if 0 {
vtkElevationFilter colorIt
  # ################################
    colorIt SetInput [plate GetOutput]
    colorIt SetLowPoint 0 0 0
    colorIt SetHighPoint 0 0 315
    colorIt SetScalarRange 0 315
  # ################################
}


# -------------- Set up the lookup table(s) ------------------------
#Step 4: Create one black white color lookup table with 256 colors 
#        and add it into the pipeline
#        Lookup table is covered in Lecture08
vtkLookupTable lut
  # ################################
  lut SetNumberOfColors 256
  lut SetValueRange 0.0 1.0
  lut SetSaturationRange 0.0 0.0
  # ################################


  
#Step 5: Comment out the single statement for adding the black 
#        white color table to the pipeline. Create another color
#        table which is not black and white 
vtkLookupTable lutBR
  # ################################
  lutBR SetNumberOfColors 256
  lutBR SetHueRange 0.67 0.0
  lutBR Build
 

	
# --------- Mapper and Acotor of the beam plate ----------------
vtkDataSetMapper plateMapper
# Map the tright dataset
    plateMapper SetInput [color GetOutput]
	
	# Step 2:
	# plateMapper SetInput [colorIt GetOutput]
	
	#Step 4 and step 5: Add the required lookup table to the pipeline
	# ################################
	#plateMapper SetLookupTable lut
	plateMapper SetLookupTable lutBR
	# ################################

#Step 10: Set the opacity “plateActor” as 0.4 or 0.5 so we 
#         can see through the beam and glyphs can be shown easily
	# ################################		
	vtkActor plateActor
	plateActor SetMapper plateMapper
	[plateActor GetProperty] SetOpacity 0.5
	
	# ################################
	
#
# ---------- Create a scalar bar actor --------------------------
#Step 8: Add a scalar bar based on your color lookup table
#        (Refer to “scalarBar.tcl” and VTK online documentation)
#        The scalar bar’s title should be “Strain”.
#        The sclar bar's value changes from 0.0 (blue) to 1.0 (red).
vtkScalarBarActor scalarBar
	# ################################
	scalarBar SetLookupTable lutBR
    scalarBar SetTitle "Strain"
    [scalarBar GetPositionCoordinate] SetCoordinateSystemToNormalizedViewport
    [scalarBar GetPositionCoordinate] SetValue 0.1 0.01
    scalarBar SetOrientationToHorizontal
    scalarBar SetWidth 0.8
    scalarBar SetHeight 0.17
	# ################################
	
	
# ----------- create the outline actor ---------------------------
#Step 0: create an outline for the beam plate
#        (Refer to "VisQuad.tcl" in Lecture06)
vtkOutlineFilter outline
    # ################################
	outline SetInput [plate GetOutput]
	# ################################
vtkPolyDataMapper outlineMapper
    # ################################
	outlineMapper SetInput [outline GetOutput]
	# ################################
vtkActor outlineActor
    # ################################
	outlineActor SetMapper outlineMapper
	# ################################


# ------------ Create the vector actor ---------------------------
#Step 9: Using vtkMaskPoints, vtkConeSource, and vtkGlyph3D to create
#        a visualization consisting of oriented glyphs representing
#        the given vector field. The number of glyphs must be less
#        than the number of vertices of the beam plate because we 
#        don’t want to clutter the display 
#        (refer to “thrshldV.tcl” in Lecture10’s sample code).
vtkThresholdPoints threshold
    # ################################
	threshold SetInput [color GetOutput]
    threshold ThresholdByUpper 0
	# ################################
vtkMaskPoints mask
    # ################################
	mask SetInput [threshold GetOutput]
    mask SetOnRatio 10
	# ################################
vtkConeSource cone
    # ################################
	cone SetResolution 3
    cone SetHeight 3
    cone SetRadius 1.0
	# ################################
vtkGlyph3D cones
    # ################################
	cones SetInput [mask GetOutput]
    cones SetSource [cone GetOutput]
    cones SetScaleFactor 0.5
    cones SetScaleModeToScaleByVector
	# ################################
vtkLookupTable lutVec
    # ################################	
	lutVec SetTableValue 0 0.9 0.9 0.9 1
    lutVec Build
	# ################################
vtkPolyDataMapper vecMapper
    # ################################
	vecMapper SetInput [cones GetOutput]
#vtkDataSetMapper vecMapper
#    vecMapper SetInput [reader GetOutput]
    vecMapper SetScalarRange 1 12
    vecMapper SetLookupTable lutVec
	# ################################
vtkActor vecActor
    # ################################
	vecActor SetMapper vecMapper
	# ################################
	

# ------------- Transform actors to right postions -----------------
# #Step 1: Transform the beam and the outline actors to a similar position and angle as the following figure so the vibration can be seen clearly
	# ################################
	vtkTransform beamTrans
	beamTrans RotateZ -180
	beamTrans RotateY 30
	beamTrans RotateX -50
	outlineActor SetUserTransform beamTrans
	plateActor SetUserTransform beamTrans
	vecActor SetUserTransform beamTrans
	
				
	# ################################


# -------- Set Renderer, RenderWindow, and WindowInteractor ---------
vtkRenderer ren1
	ren1 SetBackground 0.2 0.3 0.4
	ren1 AddActor plateActor
	ren1 AddActor outlineActor
	ren1 AddActor scalarBar
	ren1 AddActor vecActor
	


# ---------- Ajust the active camera if necessary --------------
	
	
# set thedisplay window
vtkRenderWindow renWin
	renWin SetSize 900 600
    renWin AddRenderer ren1

# Set the interactor style
vtkInteractorStyleTrackballCamera style

# Set the window interactor
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin
	iren SetInteractorStyle style
	iren AddObserver UserEvent {wm deiconify .vtkInteract}
	# Prepare for handling events. This must be called
	#   before the interactor will work.
	iren Initialize

# prevent the tk window from showing up then start the event loop
wm withdraw .

# render the image
renWin Render
after 1000


# ----------- Animate the vibration --------------------------
#Step 6: Animate the vibration in both positive and negative 
#        directions by changing the scale factor of “vtkWarpVector”
#        in [0.0, 3.0] (Call function “SetScaleFactor”)
#*********************************************************
# Using vector data "mode1"
	# ################################	
	set SignFlip 1
	set stepSize 0.1
	for {set i 0} {$i<3} {set i [expr $i + $stepSize]} {
		
		set SignFlip [expr $SignFlip * -1] 		
		warp SetScaleFactor [expr ($i * $SignFlip)]
		puts [expr ($i * $SignFlip)]
		after 200
		renWin Render
	}
    
	# ################################
after 2000


#Step 7: Repeat step 6 for vector sets “mode2”, “mode3”, “mode4”, and “mode5” separately
# Using vector data "mode2"
	# ################################
	plate SetVectorsName "mode2"
	
	for {set i 0} {$i<3} {set i [expr $i + $stepSize]} {
		
		set SignFlip [expr $SignFlip * -1] 		
		warp SetScaleFactor [expr ($i * $SignFlip)]
		puts [expr ($i * $SignFlip)]
		after 200
		renWin Render
	}
	# ################################
after 2000

# Using vector data "mode3"
	# ################################
	plate SetVectorsName "mode3"
	
	for {set i 0} {$i<3} {set i [expr $i + $stepSize]} {
		
		set SignFlip [expr $SignFlip * -1] 		
		warp SetScaleFactor [expr ($i * $SignFlip)]
		puts [expr ($i * $SignFlip)]
		after 200
		renWin Render
	}
	# ################################
after 2000

# Using vector data "mode4"
	# ################################
	plate SetVectorsName "mode4"
	
	for {set i 0} {$i<3} {set i [expr $i + $stepSize]} {
		
		set SignFlip [expr $SignFlip * -1] 		
		warp SetScaleFactor [expr ($i * $SignFlip)]
		puts [expr ($i * $SignFlip)]
		after 200
		renWin Render
	}
	# ################################
after 2000

# Using vector data "mode5"
	# ################################
	plate SetVectorsName "mode5"
	
	for {set i 0} {$i<3} {set i [expr $i + $stepSize]} {
		
		set SignFlip [expr $SignFlip * -1] 		
		warp SetScaleFactor [expr ($i * $SignFlip)]
		puts [expr ($i * $SignFlip)]
		after 200
		renWin Render
	}
	# ################################
after 2000
