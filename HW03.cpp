﻿// File: HW03_rational.cpp
//   Programming Projects 2 on page 364 (5th ed)
// Goal: test program for Rational class
// Author(s):
// Add your documentation here
////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;


/////// class definition ///////////////////////////////////////
class Rational
{
public:
	Rational( int numerator, int denominator );
	Rational( int numerator ); // sets denominator to 1
	Rational(); // sets numerator to 0, denominator to 1

	friend Rational operator + ( const Rational&, const Rational& );
	friend Rational operator - ( const Rational&, const Rational& );
	friend Rational operator * ( const Rational&, const Rational& );
	friend Rational operator / ( const Rational&, const Rational& );
	friend bool operator < (const Rational&, const Rational& );
	friend bool operator <= (const Rational&, const Rational&);
	friend bool operator > (const Rational&, const Rational&);
	friend bool operator >= (const Rational&, const Rational&);
	friend bool operator == (const Rational&, const Rational&);
	friend ostream& operator << (ostream&,  const Rational&);
	friend istream& operator >> (istream&, Rational&);
private:
	int num;
	int denom;
};

// A function to normalize the values stored so that,
//   after normalization, the denominator is positive
//   and the numerator and denominator are as small as
//   possible. For example, after normalization 4/-8 would 
//   be represented the same as -1/2
void normalize(int &num, int &denom);

//postcondition: return value is gcd of the absolute values of m and n
//depends on function int abs(int); declared in cstdlib
int gcd(int m, int n);

///////////////////////////////////////////////////////////////
// Main
// No user can modify the main function!!!
//////////////////////////////////////////////////////////////
int main()
{
	cout << "************************* CSCI B240 HW03 *******************\n";
	cout << "1.**** Testing declarations ****************\n";
	cout << "  Rational x, y(2), z(-5,-6), and w(1,-3) are;" << endl;
	Rational x, y(2), z(-5,-6), w(1,-3);
	cout << "    x = " << x << ", y = " << y << ", z = " << z << ", and w = " << w << endl;
	cout << "~~ [x = 0/1, y = 2/1, z = 5/6, and w = -1/3]" <<endl;

	cout << "2.****** Testing overloaded operator '>>' ********\n";
	cout << "  Enter a fraction in the format "	<< "integer_numerator/integer_denominator" << endl;
	cin >> x;
	cout << "  The rational you entered was assigned to x: x = " << x << endl << endl;
	
	// For testing purpose
	x = Rational(5/7);

	//cout << "3.****** Testing overloaded operator '<<' ********\n";
	//cout << z << " -  (" << w << ") = " << z - w << endl;

	cout << "3.****** Testing the constructor and normalization routines ******: " << endl;
	y =Rational(-128, -48);
	cout << "  1) y = Rational(-128, -48) outputs should be '8/3' compared with: " << y << endl;
	y =Rational(-128, 48);
	cout << "  2) y =Rational(-128, 48 ) outputs should be '-8/3' compared with: " << y << endl;
	y = Rational(128,-48);
	cout << "  3) y = Rational(128, -48) outputs should be '-8/3' compared with: " << y << endl;
	Rational a(5,-1);
	cout << "  4) 'Rational a(5,-1)' outputs should be '-5/1' compared with: " << a << endl << endl;
	//Rational ww = y*a;
	//cout <<  y << " * " << a << " = " << ww << endl;



	cout  << "\n****** Testing overloaded arithmetic operators ******\n";
	w = Rational(25,9);
	z = Rational(3,5);
	cout  << "4.****** Testing overloade '+' ******\n";
	cout << "  " << w << " + " << z << " = " << w + z << " ~ (152/45'?)" << endl << endl;

	cout  << "5.****** Testing overloade '-' ******\n";
	cout << "  " << w << " - " << z << " = " << w - z << " ~ (98/45)?" << endl << endl;

	cout  << "6.****** Testing overloade '*' ******\n";
	cout << "  " << w << " * " << z << " = " << w * z << " ~ (5/3)?" << endl << endl;
	
	cout  << "7.****** Testing overloade '/' ******\n"; 
	cout << "  " << w << " / " << z << " = " << w / z << " ~ (125/27)?" << endl << endl;


	cout  << "\n****** Testing overloaded relational operators ******\n";
	cout  << "8.****** Testing overloade '==' ******\n";
	cout << "   Is w == z?: \n";
	cout << "     Since x = " << x << " and y = " << y << ", the answer is ";
	if (x == y)
		cout << "YES." << endl;
	else
		cout << "NO." << endl << endl;

	cout  << "9.****** Testing overloaded '<' ******\n";
	cout << "  " << w << " < " << z << " = " << (w < z) << " ~ (0/F)?" << endl;
	cout << "  " << w << " < " << w << " = " << (w < w) << " ~ (0/F)?" << endl << endl;

	cout  << "10.****** Testing overloaded '<=' ******\n";
	cout << "  " << w << " <= " << z << " = " << (w <= z) << " ~ (0/F)?" << endl;
	cout << "  " << w << " <= " << w << " = " << (w <= w) << " ~ (1/T)?" << endl << endl;

	cout  << "11.****** Testing overloaded '>' ******\n";
	cout << "  " << w << " > " << z << " = " << (w > z) << " ~ (1/T)?" << endl;
	cout << "  " << w << " > " << w << " = " << (w > w) << " ~ (0/F)?" << endl << endl;

	cout  << "12.****** Testing overloaded '>=' ******\n";
	cout << "  " << w << " >= " << z << " = " << (w >= z) << " ~ (1/T)?" << endl;
	cout << "  " << w << " >= " << w << " = " << (w >= w) << " ~ (1/T)?" << endl << endl;
  
  

	// Check another set of rationals
	cout << "\n------------------------------------------------------------------\n";
	cout  << "****** Testing overloaded arithmetic operators ******\n";
	w = Rational(-21,9);
	z = Rational(3,5);
	cout  << "4.****** Testing overloade '+' ******\n";
	cout << "  " << w << " + " << z << " = " << w + z << " ~ (-26/15)?" << endl << endl;

	cout  << "5.****** Testing overloade '-' ******\n";
	cout << "  " << w << " - " << z << " = " << w - z << " ~ (-44/15)?" << endl << endl;

	cout  << "6.****** Testing overloade '*' ******\n";
	cout << "  " << w << " * " << z << " = " << w * z << " ~ (-7/5)?" << endl << endl;
	
	cout  << "7.****** Testing overloade '/' ******\n"; 
	cout << "  " << w << " / " << z << " = " << w / z << " ~ (-35/9)?" << endl << endl;


	cout  << "\n****** Testing overloaded relational operators ******\n";
	cout  << "8.****** Testing overloade '==' ******\n";
	cout << "   Is w == z?: \n";
	cout << "     Since x = " << x << " and y = " << y << ", the answer is ";
	if (x == y)
		cout << "YES." << endl;
	else
		cout << "NO." << endl << endl;

	cout  << "9.****** Testing overloaded '<' ******\n";
	cout << "  " << w << " < " << z << " = " << (w < z) << " ~ (1/T)?" << endl;
	cout << "  " << w << " < " << w << " = " << (w < w) << " ~ (0/F)?" << endl << endl;

	cout  << "10.****** Testing overloaded '<=' ******\n";
	cout << "  " << w << " <= " << z << " = " << (w <= z) << " ~ (1/T)?" << endl;
	cout << "  " << w << " <= " << w << " = " << (w <= w) << " ~ (1/T)?" << endl << endl;

	cout  << "11.****** Testing overloaded '>' ******\n";
	cout << "  " << w << " > " << z << " = " << (w > z) << " ~ (0/F)?" << endl;
	cout << "  " << w << " > " << w << " = " << (w > w) << " ~ (0/F)?" << endl << endl;

	cout  << "12.****** Testing overloaded '>=' ******\n";
	cout << "  " << w << " >= " << z << " = " << (w >= z) << " ~ (0/F)?" << endl;
	cout << "  " << w << " >= " << w << " = " << (w >= w) << " ~ (1/T)?" << endl << endl;

	return 0;
}


/////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////

//postcondition: return value is gcd of the absolute values of m and n
//depends on function int abs(int); declared in cstdlib
int gcd ( int m, int n)
{
	int t; 
	m = abs (m);  // don't care about signs.
	n = abs (n);  
	if ( n < m )  // make m >= n so algorithm will work!
	{
		t = m;
		m = n;
		n = t;
	}

	int r;
	r = m % n;
	while ( r != 0 )
	{
		r = m%n;
		m = n;  
		n = r;
	}

	return m;
}

//postcondition: n and d (to be numerator and denominator of a fraction)
//have all common factors removed, and d > 0.
void normalize( int& n, int& d)
{
   // remove common factors:
   int g = gcd( n, d);
   n = n/g; 
   d = d/g;

   //fix things so that if the fraction is 'negative'
   //it is n that carries the sign. If both n and d are 
   //negative, each is made positive.
   if ( n > 0 && d < 0 || n < 0 && d < 0 )
   {
	 n = -n;
	 d = -d;
   }
   // assert:  d > 0
}

Rational::Rational(int numerator, int denominator)
{
	normalize(numerator, denominator);
	num = numerator;
	denom = denominator;
	if (denominator == 0)
	{
		cout << "The Denominator cannot be zero." << endl;
		system("pause");
		exit(1);
	} // end if
} // end 2 Arg Constructor Rational

Rational::Rational(int numerator)
{
	num = numerator;
	denom = 1;
} // end 1 Arg Rational Constructor

Rational::Rational()
{
	num = 0;
	denom = 1;
} // end 0 Arg Constructor

istream& operator >> (istream& input, Rational& fraction)
{
	char character;

	input >> fraction.num >> character >> fraction.denom;

	if (character != '/')
	{
		cout << "Wrong input: " << character << endl;
		system("pause");
		exit(1);
	} // end if

	normalize(fraction.num, fraction.denom);
	return input;

} // end function >>

ostream& operator << (ostream& output, const Rational& fraction)
{
	output << fraction.num << '/' << fraction.denom;
	return output;
} // end function << 


bool operator == (const Rational& fraction1, const Rational& fraction2)
{
	return fraction1.num * fraction2.denom == fraction2.num * fraction1.denom;
} // end of function ==

bool operator < (const Rational& fraction1, const Rational& fraction2)
{
	return fraction1.num * fraction2.denom < fraction2.num * fraction1.denom;
} // end function <

bool operator <=(const Rational& fraction1, const Rational& fraction2)
{
	return fraction1.num * fraction2.denom <= fraction2.num * fraction1.denom;
} // end function <=

bool operator >(const Rational& fraction1, const Rational& fraction2)
{
	return fraction1.num * fraction2.denom > fraction2.num * fraction2.denom;
} // end function >

bool operator >=(const Rational& fraction1, const Rational& fraction2)
{
	return fraction1.num * fraction2.denom >= fraction2.num * fraction2.denom;
} // end function >=

Rational operator +(const Rational& fraction1, const Rational& fraction2)
{
	int n;
	int d;
	Rational addition;

	n = fraction1.num * fraction2.denom + fraction1.denom * fraction2.num;
	d = fraction1.denom * fraction2.denom;

	normalize(n, d);
	addition = Rational(n, d);

	return addition;
} // end function + 

Rational operator -(const Rational& fraction1, const Rational& fraction2)
{
	int n;
	int d;
	Rational subtraction;

	n = fraction1.num * fraction2.denom - fraction1.denom * fraction2.num;
	d = fraction1.denom * fraction2.denom;

	normalize(n, d);
	subtraction = Rational(n, d);

	return subtraction;
} // end function - 

Rational operator *(const Rational& fraction1, const Rational& fraction2)
{
	int n;
	int d;
	Rational multiplication;

	n = fraction1.num * fraction2.num;
	d = fraction1.denom * fraction2.denom;

	normalize(n, d);
	multiplication = Rational(n, d);

	return multiplication;
} // end function *

Rational operator /(const Rational& fraction1, const Rational& fraction2)
{
	int n;
	int d;
	Rational division;

	n = fraction1.num * fraction2.denom;
	d = fraction1.denom * fraction2.num;

	normalize(n, d);
	division = Rational(n, d);

	return division;
} // end function / 