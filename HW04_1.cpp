//HW04_1_startingCode.cpp

/*
* Author - Brock Pivirotto
* Date - 4/24/2016
*/

//This program defines a class for storing the names of classes
//  that a student has enrolled in.



#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;


// Class Student
class Student
{
public:
	Student();
	~Student();
	void InputData();		// Input all data from user
	void OutputData();		// Output class list to console
	void ResetClasses();	// Reset class list
	Student& operator =(const Student& rightSide);	// Assignment operator

private:
	string name;
	int numClasses;
	string *classList;
};



// ======================
//     main function
// ======================
int main()
{
	cout << "*************************** CSCI B240 Project 04 ************************\n";
	// Test our code with two student classes
	Student s1, s2;

	s1.InputData();		// Input data for student 1
	cout << "\n****** The following is the test result from the provided driver ******\n";
	cout << "Student 1's data:" << endl;
	s1.OutputData();		// Output data for student 1

	cout << endl;

	s2 = s1;
	cout << "Student 2's data after assignment from student 1:" << endl;
	s2.OutputData();		// Should output same data as for student 1

	s1.ResetClasses();
	cout << "Student 1's data after reset:" << endl;
	s1.OutputData();		// Should have no classes

	cout << "Student 2's data, should still have original classes:" << endl;
	s2.OutputData();		// Should still have original classes

	cout << endl;

	system("PAUSE");
	return 0;
} // end main


// Class Implementations
// ======================
// Student::Student
// The default constructor initialized variables to empty.
// The dynamic array is intialized to NULL.
// ======================
Student::Student()
{
	numClasses = 0;
	classList = NULL;
	name = "";
}

// ======================
// Student::~Student
// The destructor frees up any memory allocated to
// the dynamic array.
// ======================
Student::~Student()
{
	delete[] classList;

} // end destructor ~Student

// ======================
// Student::ResetClasses
// This method deletes the class list
// ======================
void Student::ResetClasses()
{

	for (int counter = 0; counter < numClasses; ++counter)
	{
		classList[counter] = '\0';
	} // end for loop
	
	numClasses = 0;
	
}// end method ResetClasses


// ======================
// Student::InputData
// This method inputs all data from the user. 
// It allows the user to enter an arbitrary number of classes
// using a dynamic array to store the strings.
// ======================
void Student::InputData()
{
	// Reset the class list just in case this method
	// was called again and the dynamic array wasn't cleared
	ResetClasses();

	cout << "Enter student name: " << endl;
	getline(cin, name);
	cout << "Enter number of classes: " << endl;
	cin >> numClasses;
	cin.ignore(2, '\n');   // Discard extra newline
	if (numClasses > 0)
	{
		// Dynamically construct array large enough to hold the
		// number of classes
		classList = new string[numClasses];
		// Loop through the number of classes, inputting
		// the name of each one into our dynamic array
		for (int i = 0; i<numClasses; i++)
		{
			cout << "Enter name of class: " << (i + 1) << endl;
			getline(cin, *(classList + i));
		}
	}

	cout << endl;
}

// ======================
// Student::OutputData
// This method outputs the data entered by the user.
// ======================
void Student::OutputData()
{
	cout << "Name: " << name << endl;
	cout << "Number of classes: " << numClasses << endl;
	

	for (int counter = 0; counter < numClasses; counter++)
	{
		cout << "Class" << " " <<(counter + 1) << ":";
		cout << classList[counter] << endl;
	} // end for loop

	cout << endl;
} // end method OutPutData

// ======================
// Student::=
// This method copies a new classList to
// the target of the assignment.  Without overloading this
// operator, we would end up with two references to the same
// class list if the assignment operator is used.
// ======================
Student& Student::operator =(const Student& strongSide)
{
	ResetClasses();		// Erase list of classes

	if (numClasses == strongSide.numClasses)
	{
		numClasses = strongSide.numClasses;

		name = strongSide.name;
		
	} 
	else
	{
		delete[] classList;

		classList = new string[strongSide.numClasses];

		numClasses = strongSide.numClasses;

		name = strongSide.name;
	} // end if/else
	
	for (int counter = 0; counter < numClasses; ++counter)
	{
		classList[counter] = strongSide.classList[counter];
	} // end for loop

	return *this;
} // end method =