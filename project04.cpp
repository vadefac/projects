// ************************* 1. Documentation ***********************************
// Project name: Robot Arm
// Project goal: Create a 3D robot arm and animate individual aspects.
// Author's name: Brock Pivirotto
// Date: 3/29/2017

// ******************* 2. Library and Header files ******************************
#include <iostream>
#include <string>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

using namespace std;

#define ESC 27
#define SPACE 32


// ******************* 3. Function prototypes *************************************
void printInteraction(void);
void init();
void myDisplay();
void reshape(int w, int h);
void myKeyboard(unsigned char key, int x, int y);
void mySpecialKeys(int key, int x, int y);
void animateFigure(void); 

// ******************* 3.1 Declaration of Variables *************************************
static GLfloat red[]={1,0,0},green[]={0,1,0}, blue[] = {0, 0, 1}; /* ,blue[]={0,0,1}; */
static int shoulder=0,elbow=0, finger=0;
static int xrot=0,yrot=0,zrot=0;
static int zElbowRotate = 0, zShoulderRotate = 0;
static GLfloat xshift=-1.0,yshift=0.0,zoom=-3.0;
static GLfloat spin = 0.0;
int animate;
int closeFingers;
string armDirection;


// ******************* 4. The main function ***************************************
// Main routine: defines window properties, creates window,
// registers callback routines and begins processing.
int main(int argc, char **argv)
{
	int winWidth = 800, winHeight = 600, winPosX = 100, winPosY = 100; // OpenGL window

	// Print user instructions for controls
	printInteraction();

	// Initialize GLUT.
	// Allow cmd line arguments to be passed to the glut
	glutInit(&argc,argv);

	// Set display mode as double-buffered and RGB color.
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);

	// GLUT Window Initialization:
	glutInitWindowSize(800,600);	// Set OpenGL window size.
	glutInitWindowPosition(100,100);
	glutCreateWindow("CSCI B365 - Project04");

	// Initialize OpenGL graphics state
	init();

	// Register callbacks:

	glutDisplayFunc(myDisplay);	// Register display routine.
	glutIdleFunc(animateFigure);
	glutReshapeFunc(reshape);		// Register reshape routine.
	glutKeyboardFunc(myKeyboard);			// Register keyboard routine.
	glutSpecialFunc(mySpecialKeys);

	// Begin processing.
	glutMainLoop();

	return 0;
}


// **************** 5. Function implementations ************************************
//Input: None
//Process: Display commands to the console
//Output: None
void printInteraction(void)
{
	cout << "Interaction:" << endl;
	cout << "Space Bar: Toggle Animation (on/off)" << endl;
	cout << "r/R: Reset the scene to its initial status" << endl;
	cout << "s/S: Positive/negative shoulder rotation about Z axis" << endl;
	cout << "e/E: Positive/negative elbow rotation about Z axis" << endl;
	cout << "o/O: Open/Close the fingers" << endl;
	cout << "c/C: Close the fingers" << endl;
	cout << "x/X: Positive/negative X-axis shift of the robot" << endl;
	cout << "y/Y: Positive/negative Y-axis shift of the robot" << endl;
	cout << "z/Z: Positive/negative robot rotation about Z-axis. " << endl;
	cout << "UP/DOWN ARROWS: (zoom in/out) Z-axis shift of the robot" << endl;
	cout << "LEFT/RIGHT ARROWS: Y-axis rotations of the robot" << endl;
	cout << "UP/DOWN ARROWS: (zoom in/out) Z-axis shift of the robot" << endl;
	cout << "ESC: exit" << endl;

}

/* some basic GL initialization */
void init()
{
	glClearColor(1.0,1.0,1.0,0.0);
}

//Input: width, height
//Process: Display call back routine
//Output: None
void myDisplay()
{
	
		glClear(GL_COLOR_BUFFER_BIT);

		

		glLoadIdentity();

		glTranslatef(0.0,0.0,-5.0);
		glPushMatrix();
			glTranslatef(xshift,yshift,zoom);
			glRotatef((GLfloat)xrot,1.0,0.0,0.0);
			glRotatef((GLfloat)yrot,0.0,1.0,0.0);
			glRotatef((GLfloat)zrot,0.0,0.0,1.0);

		glPushMatrix();
			glTranslatef(-1.0,0.0,0.0);
				glRotatef((GLfloat)zShoulderRotate,0.0,0.0,1.0);
			glTranslatef(1.0,0.0,0.0);
			glPushMatrix();
				// shoulder of the robot
				glColor3fv(red); // color of the shoulder
				// Shoulder is rotated at its own center (coordinates origin) in accordance with z axix
				glPushMatrix();
					glScalef(2.0,0.4,1.0);
					glutSolidCube(1.0); 
				glPopMatrix();

				glPushMatrix();
					glTranslatef(1.0,0.0,0.0);
						glRotatef((GLfloat)zElbowRotate,0.0,0.0,1.0);
					glTranslatef(-1.0,0.0,0.0);

					// Arm of the robot
					glColor3fv(green); // color of arm
					// Arm is rotated at its own center in accordance with z axix
					glTranslatef(2.0,0.0,0.0);
					glPushMatrix();
						glScalef(2.0,0.4,1.0);
						glutSolidCube(1.0); // arm
					glPopMatrix();

					// top finger of the robot
					glColor3fv(blue); // color of arm
					// Arm is rotated at its own center in accordance with z axix
					glTranslatef(1.5,0.06,0.1);
					glPushMatrix();
						glTranslatef(-0.5,0.0,0.0);
							glRotatef(spin,0.0,0.0,1.0);
						glTranslatef(0.5,0.0,0.0);
						glScalef(1.0,0.1,0.2);
						glutSolidCube(1.0); // arm
					glPopMatrix();

					// bottom finger of the robot
					// Arm is rotated at its own center in accordance with z axix
					glTranslatef(0.0,-0.12,0.0);
					glPushMatrix();
						glTranslatef(-0.5,0.0,0.0);
							glRotatef(-spin,0.0,0.0,1.0);
						glTranslatef(0.5,0.0,0.0);
						glScalef(1.0,0.1,0.2);
						glutSolidCube(1.0); // arm
					glPopMatrix();

			  glPopMatrix(); // end action for elbow

			glPopMatrix();

		glPopMatrix(); // end action for arms



		glPopMatrix(); // end action for entire body
		
		glutSwapBuffers();
	
}

//Input: width, height
//Process: Reshape the figure based on the window size
//Output: None
void reshape(int w, int h)
{
	GLfloat myFov = 65.0, myNear = 1.0, myFar = 30.0;
	glViewport(0,0,(GLsizei)w,(GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Projection
	gluPerspective(myFov,(GLfloat)w/(GLfloat)h, myNear, myFar);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

//Input: None
//Process: The basic keyboard controls, callback for glutKeyboardFunc() 
//Output: None

void myKeyboard(unsigned char key, int x, int y)
{
	switch(key)
	{
		case SPACE:			
			armDirection = "up";	
			if (animate == 0)
			{					
				animate = 1;
			}
			else if(animate == 1)
			{					
 				animate = 0;
			}				
			glutPostRedisplay();
				break;

		case 'r':
		spin = 0;
		xrot = 0;
		yrot = 0;
		zrot = 0;
		xshift=-1.0;
		yshift=0.0;
		zoom=-3.0;
		glutPostRedisplay();
		break;

		case 'c':
		if(closeFingers == 0)
		{
			closeFingers = 1;
		}
		else
		{
			closeFingers = 0;
		}
		glutPostRedisplay();
		break;

		case 'x':		
		xshift+=1.0;		
		glutPostRedisplay();
		break;

		case 'X':		
		xshift-=1.0;		
		glutPostRedisplay();
		break;

		case 'y':		
		yshift+=1.0;		
		glutPostRedisplay();
		break;

		case 'Y':		
		yshift-=1.0;		
		glutPostRedisplay();
		break;

		case 'z':
		zrot=(zrot-1)%360;
		glutPostRedisplay();
		break;
		case 'Z':
		zrot=(zrot+1)%360;
		glutPostRedisplay();
		break;

		case 's':
		if(zShoulderRotate > -60)
		{
			zShoulderRotate=(zShoulderRotate-1)%360;
		}
		
		glutPostRedisplay();
		break;
		case 'S':
		if(zShoulderRotate < 60)
		{
			zShoulderRotate=(zShoulderRotate+1)%360;
		}
		
		glutPostRedisplay();
		break;

		case 'e':
		if(zElbowRotate > -80)
		{
			zElbowRotate=(zElbowRotate-1)%360;
		}
		
		glutPostRedisplay();
		break;
		case 'E':
		if(zElbowRotate < 80)
		{
			zElbowRotate=(zElbowRotate+1)%360;
		}
		
		glutPostRedisplay();
		break;

		case 'o':
		if(spin < 60)
		{
			spin = spin + 2.0;
		}
		glutPostRedisplay();
		break;
		case 'O':
		if(spin > 0)
		{
			spin = spin - 2.0;
		}
		glutPostRedisplay();
		break;
				

		case ESC:
			exit(0);
			break;
		default:
			break;

	}
}

//Input: key, x, y
//Process: Display commands to the console
//Output: None
void mySpecialKeys(int key, int x, int y)
{
	
	switch(key)
	{
		    
			case GLUT_KEY_UP:
				zoom+=1;
				glutPostRedisplay();
				break;
			case GLUT_KEY_DOWN:
				zoom-=1;
				glutPostRedisplay();
				break;
			case GLUT_KEY_LEFT:
				yrot=(yrot+1)%360;
				glutPostRedisplay();
				break;
			case GLUT_KEY_RIGHT:
				yrot=(yrot-1)%360;
				glutPostRedisplay();
				break;
			case GLUT_KEY_PAGE_UP:
				xrot=(xrot-1)%360;
				glutPostRedisplay();
				break;
			case GLUT_KEY_PAGE_DOWN:
				xrot=(xrot+1)%360;
				glutPostRedisplay();
				break;
			default:
				break;		

			
	}
	
}

//Input: None
//Process: idle function so that you can click "c" and the fingers will close without having to hold "c"
//Output: None
void animateFigure(void)
{
	if(closeFingers == 1)
	{
		if(spin > 0)
		{
			spin--;
		}
		else
		{
			closeFingers = 0;
		}
		glutPostRedisplay();
	}

	if(animate == 1)
	{		

		cout << "entered" << endl;

		if (armDirection == "up")
		{
			zShoulderRotate=(zShoulderRotate+1)%360;
		}
		else if (armDirection == "down")
		{
			zShoulderRotate=(zShoulderRotate-1)%360;
		} // end if/else

		if (zShoulderRotate == 0)
		{
			armDirection = "up";	  
		}
		else if (zShoulderRotate == 60)
		{
			armDirection = "down"; 
		} // end if/else

		glutPostRedisplay();

	}

}