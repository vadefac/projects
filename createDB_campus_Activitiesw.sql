DROP DATABASE IF EXISTS theawesomedatabase;
CREATE DATABASE theawesomedatabase;
USE theawesomedatabase;

-- create the tables for the database
CREATE TABLE users (
 user_id				 INT            PRIMARY KEY   AUTO_INCREMENT,
 username			     VARCHAR(50),
 user_password			 VARCHAR(50),
 first_name			     VARCHAR(50),
 last_name			     VARCHAR(50),
 cell_number			 VARCHAR(50)
);

CREATE TABLE groups (
 group_id				 INT	PRIMARY KEY   AUTO_INCREMENT,
 user_id			     INT,
  
 CONSTRAINT user_fk_id FOREIGN KEY (user_id) REFERENCES users (user_id)
);

CREATE TABLE categories (
 category_id				 INT            PRIMARY KEY,
 category_name			     VARCHAR(50)
);

CREATE TABLE activities (
  activity_id               INT            PRIMARY KEY   AUTO_INCREMENT,
  activity_name   			VARCHAR(50)  NOT NULL,
  activity_description     	VARCHAR(50), 
  activity_date			     VARCHAR(50), 
  activity_time   	        VARCHAR(50), 
  location   	            VARCHAR(50), 
  current_status 			TINYINT(1),      
 
  category_id			    INT,
  
  CONSTRAINT category_fk_id FOREIGN KEY (category_id) REFERENCES categories (category_id)
  
);


CREATE TABLE images (
 image_id				 INT            PRIMARY KEY   AUTO_INCREMENT,
 activity_id			 INT, 
 user_id				 INT, 
 group_id				 INT,
 
 CONSTRAINT activity_fk_id FOREIGN KEY (activity_id) REFERENCES activities (activity_id),
 CONSTRAINT user_fk_id2 FOREIGN KEY (user_id) REFERENCES users (user_id),
 CONSTRAINT group_fk_id2 FOREIGN KEY (group_id) REFERENCES groups (group_id)
);

CREATE TABLE sms (
  sms_id				 INT	PRIMARY KEY   AUTO_INCREMENT,
  dt_created 		     DATE,
  user_id                INT, 
  content		     	 VARCHAR(250),  
  
  CONSTRAINT user_fk_id3 FOREIGN KEY (user_id) REFERENCES users (user_id) 
);

CREATE TABLE passwords (  
 password_id		   INT	PRIMARY KEY   AUTO_INCREMENT,
 password_name		   VARCHAR(45)  
);



-- Insert data into the tables
INSERT INTO  users VALUES
(1,'a','b' ,'v' ,'c' ,'s');

INSERT INTO passwords  VALUES
(1,'sesame');

INSERT INTO categories  VALUES
(1,'General'),
(2,'Major Events'),
(3,'Clubs');

INSERT INTO groups  VALUES
(1,1);

INSERT INTO activities VALUES
(1,'SSAB Movie Night', 'Come out and join the Sand Shark Activities Board for movie night! The SSAB will be presenting 
					  	Bee Movie. Dont miss this amazing movie!', '4/3/17','8:30PM - 10:30PM' ,'BFT USCB Campus Center 105' , 1,3),
(2,'SSAB Comedy Night', 'Come out and join the Sand Shark Activities Board for comedy night! The SSAB will be presenting
					  	Dulce Sloan', '4/5/17','7:30 - 9:30PM' ,'BFT USCB Campus Center 105' , 1 ,3),
(3,'Spring Performance of Angtigone', 'Come out and enjoy the theatre performance of Angtigone!', '4/12/17','8:00PM – 11:00PM' ,'BFT USCB Campus Center 105' , 1 ,3),
(4,'Gamma Beta Phi Member Meeting', 'Gamma Beta Phi members are encouraged to come out and support your local GBP chapter.', '4/4/17','6:00PM - 6:00PM' ,'USCB South Campus Library 267' , 1,1),
(5,'Environmental Club Meeting', 'Environmental Club Meeting', '4/4/17','7:00PM - 7:30PM' ,'Lazy Lounge' , 1 ,1),
(6,'Society of Creative Writers', 'USCBs Society of Creative Writers is dedicated to sharing a passion for the written word with like-minded peers.', 'First Thursday of every month','5:30PM - 6:30PM' ,'Hargray 165' , 1 ,1),
(7,'USCB Art Club', 'The Art Club provides a venue for a broad student population to discuss, plan, and participate, in various art related activities both within and outside of the University.', 'Fridays','6:30PM - 7:30PM' ,'Hargray 162' , 1 ,1),
(8,'Rogues and Vacaboundes, the USCB Drama Club', 'This organization is for students who are interested in any aspect of theatre including acting, directing, and design!', '4/7/17','5:30PM - 6:30PM' ,'Historic Beaufort Campus, Center for the Arts Auditorium' , 1 ,1),
(9,'Psychology Club', 'The Psychology Club provides students of the social sciences with opportunities for academic and professional development.', '4/10/17','3:30PM - 4:30PM' ,'Hargray 165' , 1 ,1),
(10,'Education Club', 'The Education Club was founded to provide education majors with opportunities of growth and enhancement in the field of education as well as providing service to the community.', '4/7/17','5:30PM - 6:30PM' ,'Hargray 162' , 1 ,1),
(11,'5th Annual Health and Wellness Fair', 'Come out and join the Health Promotion program for the Health and Wellness Fair!
					  	The USCB Health and Wellness Fair is designed to increase health awareness for the 
					  	community. The event is free and open to the public and will feature more than 30 
					  	vendors including Beaufort Memorial Hospital – LifeFit Wellness Unit, One Blood, 
					  	ComForCare Home Care, Palmetto Running Company and many more. In addition, there 
					  	will be free blood pressure screenings, blood sugar checks, live bio and neurofeedback 
					  	tests and vision and glaucoma screenings. The activities include live music, a 
					  	rock climbing wall, an obstacle course, corn hole and fitness challenges. 
					  	Additionally, there will be giveaways every 15 minutes.', '3/30/17','11:00AM - 2:00PM' ,'USCB South Campus Library Lawn' , 0 ,2),
(12,'8th Annual Student Research and Scholarship Day', 'Do not miss the 8th Annual Student Research and Scholarship day. Student-based research
					  	is showcased to faculty and students. Awards are given to three projects per field.  ', '4/17/17','12:00PM - 5:00 PM' ,'USCB Hargray Lobby' , 1 ,2),
(13,'Class of 2017 Commencement', 'Do not miss the Class of 2017 commencement ceremony! Come watch and support your 
					  	upperclassmen as they walk across the stage and begin a new chapter in their lives.', '4/28/17','6:00PM - 8:00PM' ,'USCB South Campus' , 1 ,2);

 -- create the users and grant priveleges to those users
GRANT SELECT, INSERT, DELETE, UPDATE
ON theawesomedatabase.*
TO mgs_user@localhost
IDENTIFIED BY 'pa55word';

GRANT SELECT
ON activities
TO mgs_tester@localhost
IDENTIFIED BY 'pa55word';
 
 
 

