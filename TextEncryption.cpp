#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;


void encrypt(char text[]);
void decrypt(char text[]);

char text[40];
string wholetext;
string encryptedtext;
string originaltext;
int caesarkey1, caesarkey2;
int pattern[4];
int shiftarray = 0;
int j = 1;


int main()
{
	

	cout << "Enter the digits for Caesar key 1, and Caesar key 2" << endl;
	cin >> caesarkey1 >> caesarkey2;	
	
	cout << "Enter the pattern (Example: 1 1 1 1 2, 2 2 1 2 1)" << endl;
	cin >> pattern[0] >> pattern[1] >> pattern[2] >> pattern[3] >> pattern[4];
	
	shiftarray = 0;	
	
	fstream inputStream;
	inputStream.open("text.txt");	
	inputStream >> std::noskipws;

	for (int i = 0; i < 40; i++)
	{
		inputStream >> text[i];		
	} // end for
	inputStream.close();
	
	originaltext = text;
	cout << "Original Text: " << originaltext << endl;

	encrypt(text);
	encryptedtext = text;
	cout << "Encrypted Text: " << encryptedtext << endl;

	shiftarray = 0;

	decrypt(text);
	wholetext = text;

	cout << "Decrypted Text: " << wholetext << endl;

} // end main



void encrypt(char text[])
{
	
	for (int i = 0; i < 40; i++)
	{
		
		if(i % 5 == 0 && i != 0)
		{
			shiftarray += 5;
		}

		if (pattern[i - shiftarray] == 1)
		{
			text[i] -= caesarkey1;					
		}
		else if (pattern[i - shiftarray] == 2)			
		{
			text[i] -= caesarkey2;				
		}	
	} 	
}

void decrypt(char text[])
{	
	for (int i = 0; i < 40; i++)
	{
		if(i % 5 == 0 && i != 0)
		{
			shiftarray += 5;
		}

		if (pattern[i - shiftarray] == 1)
		{
			text[i] += caesarkey1;		
		}
		else if(pattern[i - shiftarray] == 2)
		{
			text[i] += caesarkey2;			
		}
	} 	
}