// ************************* 1. Documentation ***********************************
// Project name: 3D Room
// Project goal: Create a 3D room with lighting
// Author's name: Brock Pivirotto
// Date: 4/03/2017

// ******************* 2. Library and Header files ******************************
#include <iostream>
#include <string>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

using namespace std;

#define ESC 27
#define SPACE 32


// ******************* 3. Function prototypes *************************************
void printInteraction(void);
void init();
void myDisplay();
void reshape(int w, int h);
void myKeyboard(unsigned char key, int x, int y);
void mySpecialKeys(int key, int x, int y);
void animateFigure(void); 
void mouse (int button, int state, int x, int y) ;
void motion (int x, int y) ;

// ******************* 3.1 Declaration of Variables *************************************
static GLfloat red[]={0.25,0,0},green[]={0,1,0}, blue[] = {0, 0, 1}, lightblue[] = {0.04, 0.71, 1},
	gray[] = {0.86, 0.86, 0.86}, brown[] = {0.45, 0.29, 0.07}, white[]={1,1,1}, violet[]={0.82,0.13,0.56}; /* ,blue[]={0,0,1}; */

static int xrot=0,yrot=0,zrot=0;

static GLfloat xshift=-1.0,yshift=0.0,zoom=-3.0;
static GLfloat spin = 0.0;
int animate;
int lightOn = 1;
int spotlight = 1;
int fanOn = 0;

int fanmovement;
string armDirection;


float previousX = 0.0f, previousY = 0.0f, previousZ = 0.0f;


bool leftButton = false;
bool rightButton = false;
bool middleButton = false;

GLfloat light_diffuse1[] = {1.0, 1.0, 1.0, 1.0 }; // light on
GLfloat light_diffuse2[] = {0.0, 0.0, 0.0, 0.0 }; // light off


float lightAmb[] = { 0.0, 0.0, 0.0, 1.0 };
	float lightDifAndSpec[] = { 1.0, 1.0, 1.0, 1.0 };
	float globAmb[] = { 0.05, 0.05, 0.05, 1.0 };

// ******************* 4. The main function ***************************************
// Main routine: defines window properties, creates window,
// registers callback routines and begins processing.
int main(int argc, char **argv)
{
	int winWidth = 800, winHeight = 600, winPosX = 100, winPosY = 100; // OpenGL window

	// Print user instructions for controls
	printInteraction();

	// Initialize GLUT.
	// Allow cmd line arguments to be passed to the glut
	glutInit(&argc,argv);

	// Set display mode as double-buffered and RGB color.
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);

	// GLUT Window Initialization:
	glutInitWindowSize(800,600);	// Set OpenGL window size.
	glutInitWindowPosition(100,100);
	glutCreateWindow("CSCI B365 - Project04");

	//glEnable(GL_DEPTH_TEST); 

	// Initialize OpenGL graphics state
	init();

	// Register callbacks:

	glutDisplayFunc(myDisplay);	// Register display routine.
	glutIdleFunc(animateFigure);
	glutReshapeFunc(reshape);		// Register reshape routine.
	glutKeyboardFunc(myKeyboard);			// Register keyboard routine.
	glutSpecialFunc(mySpecialKeys);
	glutMouseFunc (mouse);       
	glutMotionFunc (motion);  
	// Begin processing.
	glutMainLoop();

	return 0;
}


// **************** 5. Function implementations ************************************
//Input: None
//Process: Display commands to the console
//Output: None
void printInteraction(void)
{
	cout << "Interaction:" << endl;
	cout << "Space Bar: Toggle Animation (on/off)" << endl;
	cout << "r/R: Reset the scene to its initial status" << endl;
	cout << "PAGE UP/PAGE DOWN: Change the viewing directions towards the ceiling/floor" << endl;
	cout << "  Press and drag right mouse button to translate." << endl;
	cout << "  Press and drag middle mouse button to scale." << endl;
    cout << "  Press and drag left mouse button to rotate." << endl;
	cout << "UP/DOWN ARROWS: (zoom in/out) Z-axis shift of the room" << endl;
	cout << "LEFT/RIGHT ARROWS: Y-axis rotations of the room" << endl;	
	cout << "Press o/c to turn on and off the light" << endl;
	cout << "ESC: exit" << endl;

}

/* some basic GL initialization */
void init()
{
	glClearColor(1.0,1.0,1.0,0.0);
	GLfloat light_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
	


   GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	
   GLfloat light_position[] = { 1.0, 7.0, 1.0, 0.0 };
   

   // Set light 0's color and position
   glLightfv (GL_LIGHT0, GL_AMBIENT, light_ambient);
   glLightfv (GL_LIGHT0, GL_DIFFUSE, light_diffuse1);
   glLightfv (GL_LIGHT0, GL_SPECULAR, light_specular);
   glLightfv (GL_LIGHT0, GL_POSITION, light_position);
   glLightfv (GL_LIGHT0, GL_EMISSION, light_position);
   glEnable (GL_LIGHTING);
   glEnable (GL_LIGHT0);

   // Set light 1's color and position
   glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmb);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDifAndSpec);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightDifAndSpec);
	glEnable(GL_LIGHT1); 

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globAmb);
	
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE); 
    
   glEnable(GL_DEPTH_TEST);
   glEnable(GL_COLOR_MATERIAL); 

}

//Input: None
//Process: draw the objects on screen
//Output: None
void myDisplay()
{

	float lightPos[] = { 0.0, 3.0, 0.0, 1.0 }; // Spotlight position.
	float spotDirection[] = {0.0, -1.0, 0.0}; // Spotlight direction.   
	
		glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);

		if(lightOn == 1)
		{
			 glLightfv (GL_LIGHT0, GL_DIFFUSE, light_diffuse1);
		}
		else if(lightOn == 0)
		{
			 glLightfv (GL_LIGHT0, GL_DIFFUSE, light_diffuse2);
		}
		if(spotlight == 1)
		{
			 glLightfv (GL_LIGHT1, GL_DIFFUSE, lightDifAndSpec);
		}
		else if(spotlight == 0)
		{
			 glLightfv (GL_LIGHT1, GL_DIFFUSE, light_diffuse2);
		}

		glLoadIdentity();

		gluLookAt (0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

		glTranslatef(0.0,0.0,-5.0);
		glPushMatrix();
			glTranslatef(xshift,yshift,zoom);
			glRotatef((GLfloat)xrot,1.0,0.0,0.0);
			glRotatef((GLfloat)yrot,0.0,1.0,0.0);
			glRotatef((GLfloat)zrot,0.0,0.0,1.0);

		glPushMatrix();
			glTranslatef(-1.0,0.0,0.0);
				
			glPushMatrix();
				
				glColor3fv(brown); 				
				glPushMatrix();
					glTranslatef(-1.0,0.0,0.0);
					glScalef(0.4,4.0,6.0);
					glutSolidCube(1.0); 
				glPopMatrix();

				
				glColor3fv(brown); 					
					glTranslatef(5.0,0.0,0.0);
					glPushMatrix();
						glScalef(0.4,4.0,6.0);
						glutSolidCube(1.0); 
					glPopMatrix();

					glColor3fv(red); 					
					glTranslatef(-3.0,-1.8,0.0);
					glPushMatrix();
						glScalef(6.0,0.4,6.0);
						glutSolidCube(1.0);
					glPopMatrix();

					glColor3fv(brown); 					
					glTranslatef(0.0,1.8,-2.8);
					glPushMatrix();
						glScalef(6.0,4.0,0.4);
						glutSolidCube(1.0); 
					glPopMatrix();

					glColor3fv(gray); 					
					glTranslatef(0.0,1.9,2.8);
					glPushMatrix();
						glScalef(6.4,0.4,6.0);
						glutSolidCube(1.0); 
					glPopMatrix();
					

					glColor3fv(white); 					
					glTranslatef(0.0,1.8,2.8);
					glPushMatrix();
						glTranslated(0, -2.0 ,-3.0); 
						glRotatef(90,1.0,0.0,0.0);
						GLUquadricObj * qobj;
						qobj = gluNewQuadric();						
						gluCylinder(qobj, 0.1, 0.1, 0.8, 18,8);
					glPopMatrix();

					glPushMatrix();
					    glTranslatef(0.0,0.0,-3.0);
						glRotatef(fanmovement,0.0,1.0,0.0);	
						glTranslatef(0.0,0.0,3.0);
						glColor3fv(white); 					
						glTranslatef(0.0,-2.7,-3.0);
						glPushMatrix();
							glScalef(0.4,0.1,1.8);
							glutSolidCube(1.0); 
						glPopMatrix();

						
						glPushMatrix();
							glScalef(1.8,0.1,0.4);
							glutSolidCube(1.0); 
						glPopMatrix();

						glTranslatef(0.0,-0.4,0.0);
						glPushMatrix();							
							glutSolidSphere(0.4,18,8);
						glPopMatrix();
						
					glPopMatrix();

					glColor3fv(violet); 		
					glPushMatrix();
							glTranslatef(0.0,-5.0,-4.0);
							glutSolidTeapot(0.5);
						glPopMatrix();		

			 

			glPopMatrix();

		glPopMatrix(); 



		glPopMatrix(); 
		
		glutSwapBuffers();
	
}

//Input: width, height
//Process: Reshape the figure based on the window size
//Output: None
void reshape(int w, int h)
{
	GLfloat myFov = 65.0, myNear = 1.0, myFar = 30.0;
	glViewport(0,0,(GLsizei)w,(GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Projection
	gluPerspective(myFov,(GLfloat)w/(GLfloat)h, myNear, myFar);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

//Input: key, x, y
//Process: The basic keyboard controls, callback for glutKeyboardFunc() 
//Output: None
void myKeyboard(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 'r':		
	case'R':
		 lightOn = 1;
		 spotlight = 1;
		 fanOn = 0;
		 fanmovement = 0;
		 xshift=-1.0;
		 yshift=0.0;
		 zoom=-3.0;
		 xrot=0;
		 yrot=0;
		 zrot=0;

		glutPostRedisplay();
		break;
		
		case '0':
		if(lightOn == 1)
		{
			lightOn = 0;
		}
		else if(lightOn == 0)
		{
			lightOn = 1;
		}
		glutPostRedisplay();
		break;

		case 'o':		
			
			spotlight = 1;
		
		glutPostRedisplay();
		break;

		case 'c':		
			spotlight = 0;
		
			
		
		glutPostRedisplay();
		break;

		case SPACE:
		if(fanOn == 1)
		{
			fanOn = 0;
		}
		else if(fanOn == 0)
		{
			fanOn = 1;
		}
		glutPostRedisplay();
		
		break;

		

		case ESC:
			exit(0);
			break;
		default:
			break;

	}
}

//Input: key, x, y
//Process: Special keyboard controls
//Output: None
void mySpecialKeys(int key, int x, int y)
{
	
	switch(key)
	{
		    
			case GLUT_KEY_UP:
				zoom+=1;
				glutPostRedisplay();
				break;
			case GLUT_KEY_DOWN:
				zoom-=1;
				glutPostRedisplay();
				break;
			case GLUT_KEY_LEFT:

				yrot=(yrot-1)%360;
				glutPostRedisplay();
				
				break;
			case GLUT_KEY_RIGHT:
				yrot=(yrot+1)%360;
				glutPostRedisplay();
				break;
			case GLUT_KEY_PAGE_UP:
				xrot=(xrot-1)%360;
				glutPostRedisplay();
				break;
			case GLUT_KEY_PAGE_DOWN:
				xrot=(xrot+1)%360;
				glutPostRedisplay();
				break;
			default:
				break;				
	}
	
}

//Input: None
//Process: idle function so that you can click "space" and the fan will rotate
//Output: None
void animateFigure(void)
{
	if(fanOn == 1)
	{
		fanmovement += 5;
	}

	  glutPostRedisplay();

	

}

//Input: button, state, x, y
//Process: Mouse controls
//Output: None
void mouse (int button, int state, int x, int y) 
{	
	// checks if the user presses the left button
	if (button == GLUT_LEFT_BUTTON) 
	{
		leftButton = state == GLUT_DOWN;
	} 

	if (button == GLUT_RIGHT_BUTTON) 
	{
		rightButton = state == GLUT_DOWN;
	} 

	if (button == GLUT_MIDDLE_BUTTON) 
	{
		middleButton = state == GLUT_DOWN;
	} 	
	
	previousX = x;
	previousY = y;
	previousZ = y;
}

//Input:  x, y
//Process: Based on mouse input, move the room
//Output: None
void motion (int x, int y) 
{
	//  Transformation
	if (rightButton) 
	{
		xshift += (x - previousX) / 100 ;
		yshift += -(y - previousY ) / 100;
	}	
	//  Scale
	if (middleButton) 
	{		
		zoom -= (float)(y - previousY) / 100.0f;		
	}	
	//  Rotate
	if (leftButton) 
	{
		xrot += y - previousY;
		yrot += x - previousX;
	}	

	// reset the previousX, previousY, and previousZ to the current position of the mouse
	previousX = x;
	previousY = y;
	previousZ = y;

	glutPostRedisplay();
}